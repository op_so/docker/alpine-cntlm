# hadolint ignore=DL3007
FROM alpine:latest

ARG VERSION
ARG BUILD_DATE
ARG VCS_REF

ENV container docker

LABEL maintainer="FX Soubirou <soubirou@yahoo.fr>" \
    org.opencontainers.image.title="alpine-cntlm" \
    org.opencontainers.image.description="A lightweight automatically updated alpine cntlm image" \
    org.opencontainers.image.authors="FX Soubirou <soubirou@yahoo.fr>" \
    org.opencontainers.image.licenses="MIT" \
    org.opencontainers.image.version="${VERSION}" \
    org.opencontainers.image.url="https://hub.docker.com/r/jfxs/alpine-cntlm" \
    org.opencontainers.image.source="https://gitlab.com/op_so/docker/alpine-cntlm" \
    org.opencontainers.image.revision=${VCS_REF} \
    org.opencontainers.image.created=${BUILD_DATE}

COPY files/start.sh /usr/local/bin/

# hadolint ignore=DL3018
RUN apk --no-cache add cntlm \
    && chmod 755 /usr/local/bin/start.sh \
    && chmod -R g=u /etc/passwd

USER 10010
EXPOSE 3128

ENTRYPOINT ["/usr/local/bin/start.sh"]
