** Settings ***
Documentation     start.sh tests
...
...               A test suite to validate start shell script.

Library           Process

*** Variables ***
${TESTS_DIR}   tests
${SHELL_DIR}   files
${SHELL.SH}   start.sh
${USAGE_DISPLAY}   Usage: ${SHELL.SH} -i

*** Test Cases ***
with no option
    [Tags]    start.sh    simple
    ${result} =    When Run Process    ${SHELL_DIR}/${SHELL.SH}
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stdout}    -u argument is required
    And Should Contain    ${result.stdout}    Required arguments are missing!

with unknown option in run mode
    [Tags]    start.sh    simple
    ${result} =    When Run Process    ${SHELL_DIR}/${SHELL.SH}   -d   domain   -u   username   -p   password   -x
    Then Should Contain    ${result.stderr}    Illegal option -x
    And Run cntlm    ${result.rc}    ${result.stdout}    ${result.stderr}

[-u] option missing in run mode
    [Tags]    start.sh    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -d   domain   -p   password
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stdout}    -u argument is required
    And Should Contain    ${result.stdout}    Required arguments are missing!

[-u] option with missing argument in run mode
    [Tags]    image-to-badge   simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -d   domain   -p   password    -u
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stdout}    -u argument is required
    And Should Contain    ${result.stdout}    Required arguments are missing!

[-p] option missing in run mode
    [Tags]    start.sh    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -d   domain   -u   username
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stdout}    -p argument is required
    And Should Contain    ${result.stdout}    Required arguments are missing!

[-p] option with missing argument in run mode
    [Tags]    image-to-badge   simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -d   domain   -u   username    -p
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stdout}    -p argument is required
    And Should Contain    ${result.stdout}    Required arguments are missing!

[-d] option missing in run mode
    [Tags]    start.sh    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}    -u   username   -p   password
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stdout}    -d argument is required
    And Should Contain    ${result.stdout}    Required arguments are missing!

[-d] option with missing argument in run mode
    [Tags]    image-to-badge   simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}    -u   username   -p   password    -d
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stdout}    -d argument is required
    And Should Contain    ${result.stdout}    Required arguments are missing!

Successfull start in run mode
    [Tags]    start.sh    simple
    ${result} =    When Run Process    ${SHELL_DIR}/${SHELL.SH}   -d   domain   -u   username   -p   password
    Then Run cntlm    ${result.rc}    ${result.stdout}    ${result.stderr}

with unknown option in hash mode
    [Tags]    start.sh    simple
    ${result} =    When Run Process    ${SHELL_DIR}/${SHELL.SH}    -H   -d   domain   -u   username   -x
    Then Should Contain    ${result.stderr}    Illegal option -x
    And Run cntlm    ${result.rc}    ${result.stdout}    ${result.stderr}

[-u] option missing in hash mode
    [Tags]    start.sh    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}    -H   -d   domain
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stdout}    -u argument is required
    And Should Contain    ${result.stdout}    Required arguments are missing!

[-u] option with missing argument in hash mode
    [Tags]    image-to-badge   simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}    -H   -d   domain    -u
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stdout}    -u argument is required
    And Should Contain    ${result.stdout}    Required arguments are missing!

[-d] option missing in hash mode
    [Tags]    start.sh    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}    -H    -u   username
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stdout}    -d argument is required
    And Should Contain    ${result.stdout}    Required arguments are missing!

[-d] option with missing argument in hash mode
    [Tags]    image-to-badge   simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}    -H    -u   username    -d
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stdout}    -d argument is required
    And Should Contain    ${result.stdout}    Required arguments are missing!

Successfull launch in hash mode
    [Tags]    start.sh    simple
    ${result} =    When Run Process    ${SHELL_DIR}/${SHELL.SH}    -H    -d   domain   -u   username
    Then Run cntlm    ${result.rc}    ${result.stdout}    ${result.stderr}

Successfull launch in help mode
    [Tags]    start.sh    simple
    ${result} =    When Run Process    ${SHELL_DIR}/${SHELL.SH}    -h
    Then Should Contain    ${result.stdout}    Usage: start.sh <options/arguments>
    And Run cntlm    ${result.rc}    ${result.stdout}    ${result.stderr}

*** Keywords ***
Run cntlm
    [Arguments]    ${rc}    ${result.stdout}    ${stderr}
    Should Be Equal As Integers    ${rc}    127
    Should Contain    ${stderr}    cntlm: not found
