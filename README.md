# jfxs / alpine-cntlm

[![Software License](https://img.shields.io/badge/license-MIT-informational.svg?style=flat)](LICENSE)
[![Pipeline Status](https://gitlab.com/op_so/docker/alpine-cntlm/badges/main/pipeline.svg)](https://gitlab.com/op_so/docker/alpine-cntlm/pipelines)

A [cntlm](https://cntlm.sourceforge.net/) Docker image:

* **lightweight** image based on Alpine Linux only 3 MB,
* multiarch with support of **amd64** and **arm64**,
* **non-root** container user,
* **automatically** updated by comparing SBOM changes,
* image **signed** with [Cosign](https://github.com/sigstore/cosign),
* an **SBOM attestation** added using [Syft](https://github.com/anchore/syft),
* available on **Docker Hub** and **Quay.io**.

[![GitLab](https://shields.io/badge/Gitlab-informational?logo=gitlab&style=flat-square)](https://gitlab.com/op_so/docker/alpine-cntlm) The main repository.

[![Docker Hub](https://shields.io/badge/dockerhub-informational?logo=docker&logoColor=white&style=flat-square)](https://hub.docker.com/r/jfxs/alpine-cntlm) The Docker Hub registry.

[![Quay.io](https://shields.io/badge/quay.io-informational?logo=docker&logoColor=white&style=flat-square)](https://quay.io/repository/ifxs/alpine-cntlm) The Quay.io registry.

## Getting Started

### Generating your password hash

In order to generate your password hash (-H must be the first argument):

```shell
docker run --rm -it jfxs/alpine-cntlm -H -u <username> -d <domain> <proxy_host>:<proxy_port>
```

Enter your password (not displayed) and press enter.

```shell
PassLM          8B536635BF212BDF23E58C193159813A
PassNT          CA739B6BCB33AEB40B377AA52DD37D94
PassNTLMv2      82C73BA04D05533DD9BD9B3930B41977    # Only for user ...
```

### Running the cntlm proxy

Get the PassNTLMv2 hash generated previously and run:

```shell
docker run -d --rm --name proxy -p 3128:3128 jfxs/alpine-cntlm  \
-u <username> -p 82C73BA04D05533DD9BD9B3930B41977 -d <domain> <proxy_host>:<proxy_port>
```

If necessary, the corporate DNS servers can be set-up:

```shell
docker run -d --rm --name proxy --dns 10.x.y.z --dns 10.u.v.w -p 3128:3128 jfxs/alpine-cntlm ...
```

### Arguments and options of cntlm

| Arg. / Opt. | Description                                            | Default                              |
| ----------- | ------------------------------------------------------ | ------------------------------------ |
| `-u`        | Proxy account/user name.                               | **Required**                         |
| `-d`        | Proxy account domain/workgroup name.                   | **Required**                         |
| `-p`        | NTLMv2 Hash, required for running the proxy.           | **Required**                         |
| `-H`        | Option to get NTLMv2 hash, must be the first argument. | -                                    |
| `-w`        | Workstation NetBIOS name.                              | `cntlm`                              |
| `-g`        | Gateway mode, to desactivate: `-g no`                  | `yes`                                |
| `-l`        | Local port for the cntlm proxy service.                | `3128`                               |
| `-N`        | Direct access networks, separated by commas.           | `localhost,127.0.0.*,10.*,192.168.*` |
| `-v`        | Option for debugging information.                      | -                                    |
| `-h`        | Print usage, must be the first argument.               | -                                    |

## Built with

Docker latest tag is [--VERSION--](https://gitlab.com/op_so/docker/alpine-cntlm/-/blob/main/Dockerfile) and contains:

--SBOM-TABLE--

Details are updated on [Dockerhub Overview page](https://hub.docker.com/r/jfxs/alpine-cntlm) when an image is published.

## Versioning

The Docker tag is defined by the alpine version used and an increment to differentiate build with the same alpine version:

```text
<alpine_version>-<increment>
```

Example: 3.16.3-1

## Signature and attestation

[Cosign](https://github.com/sigstore/cosign) public key:

```shell
-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEa3yV6+yd/l4zh/tfT6Tx+zn0dhy3
BhFqSad1norLeKSCN2MILv4fZ9GA6ODOlJOw+7vzUvzZVr9IXnxEdjoWJw==
-----END PUBLIC KEY-----
```

The public key is also available online: <https://gitlab.com/op_so/docker/cosign-public-key/-/raw/main/cosign.pub>.

To verify an image:

```shell
cosign verify --key cosign.pub $IMAGE_URI
```

To verify and get the SBOM attestation:

```shell
cosign verify-attestation --key cosign.pub --type spdxjson $IMAGE_URI | jq '.payload | @base64d | fromjson | .predicate'
```

## Authors

* **FX Soubirou** - *Initial work* - [GitLab repositories](https://gitlab.com/op_so)

## License

This program is free software: you can redistribute it and/or modify it under the terms of the MIT License (MIT). See the [LICENSE](https://opensource.org/licenses/MIT) for details.
