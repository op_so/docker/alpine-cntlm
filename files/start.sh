#!/bin/sh
#
# Copyright (c) 2022 FX Soubirou
# MIT License (MIT) - https://opensource.org/licenses/MIT

export PATH="$PATH:/usr/sbin"

USERNAME=""
PASSWORD=""
DOMAIN=""
MANDATORY_ARGUMENT=1

DEFAULT_LISTEN="3128"
DEFAULT_NO_PROXY="localhost,127.0.0.*,10.*,192.168.*"
DEFAULT_WORKSTATION="cntlm"

GATEWAY=""
LISTEN=""
NO_PROXY=""
WORKSTATION=""

display_usage() {
    echo "Usage: start.sh <options/arguments>"
    echo ""
    echo "Arg. / Opt.:"
    echo "  -u Proxy account/user name (Required)"
    echo "  -d Proxy account domain/workgroup name (Required)"
    echo "  -p NTLMv2 Hash, required for running the proxy (Required)"
    echo "  -H Option to get NTLMv2 hash, must be the first argument."
    echo "  -w Workstation NetBIOS name."
    echo "  -g Gateway mode, to desactivate: -g no"
    echo "  -l Local port for the cntlm proxy service."
    echo "  -N Direct access networks, separated by commas."
    echo "  -v Option for debugging information."
    echo "  -h Print usage, must be the first argument."
    echo ""
}

test_argument() {
  if [ -z "$2" ]; then
    printf "\033[0;31m[ERROR]: -%s argument is required !\033[0m\n" "$1"
    MANDATORY_ARGUMENT=0
  fi
}

get_arg_init() {
  test_argument "u" "$USERNAME"
  test_argument "d" "$DOMAIN"
}

get_arg_run() {
  test_argument "u" "$USERNAME"
  test_argument "p" "$PASSWORD"
  test_argument "d" "$DOMAIN"
}

if ! whoami >/dev/null 2>&1; then
  if [ -w /etc/passwd ]; then
    echo "${USER_NAME:-default}:x:$(id -u):0:${USER_NAME:-default} user:${HOME}:/sbin/nologin" >>/etc/passwd
  fi
fi

while getopts d:gHhl:N:p:u:vw: option; do
  case "${option}" in
  d)
    DOMAIN="${OPTARG}"
    ;;
  g)
    GATEWAY="${OPTARG}"
    ;;
  H) ;;
  h) ;;
  l)
    LISTEN="${OPTARG}"
    ;;
  N)
    NO_PROXY="${OPTARG}"
    ;;
  p)
    PASSWORD="${OPTARG}"
    ;;
  u)
    USERNAME="${OPTARG}"
    ;;
  v) ;;
  w)
    WORKSTATION="${OPTARG}"
    ;;
  *) ;;

  esac
done

if [ "$1" = "-h" ]; then
  display_usage
elif [ "$1" = "-H" ]; then
  get_arg_init
else
  get_arg_run
fi

if [ "$MANDATORY_ARGUMENT" = "0" ]; then
  echo "Required arguments are missing!"
  exit 1
fi

if [ "$GATEWAY" != "no" ]; then
  OPT_GATEWAY="-g"
fi

if [ "$LISTEN" = "" ]; then
  ARG_LISTEN="-l $DEFAULT_LISTEN"
fi

if [ "$NO_PROXY" = "" ]; then
  ARG_NO_PROXY="-N $DEFAULT_NO_PROXY"
fi

if [ "$WORKSTATION" = "" ]; then
  ARG_WORKSTATION="-w $DEFAULT_WORKSTATION"
fi

if [ "$1" = "-h" ]; then
  set -- cntlm -h
elif [ "$1" = "-H" ]; then
  set -- cntlm "$ARG_WORKSTATION" "$@"
else
  set -- cntlm -f "$OPT_GATEWAY" "$ARG_LISTEN" "$ARG_NO_PROXY" "$ARG_WORKSTATION" "$@"
fi

exec "$@"
